use std::cmp::max;
use std::collections::VecDeque;
use std::future::Future;
use std::pin::Pin;
use std::task::Context;
use std::task::Poll;
use std::time::Duration;

use futures::Stream;

use telegram_bot_raw::{AllowedUpdate, GetUpdates, Integer, Update};

use crate::api::Api;
use crate::errors::Error;

const TELEGRAM_LONG_POLL_TIMEOUT_SECONDS: u64 = 5;
const TELEGRAM_LONG_POLL_LIMIT_MESSAGES: Integer = 100;
const TELEGRAM_LONG_POLL_ERROR_DELAY_MILLISECONDS: u64 = 500;

/// This type represents stream of Telegram API updates and uses
/// long polling method under the hood.
#[must_use = "streams do nothing unless polled"]
pub struct UpdatesStream {
    api: Api,
    last_update: Integer,
    buffer: VecDeque<Update>,
    timeout: Duration,
    allowed_updates: Vec<AllowedUpdate>,
    limit: Integer,
    error_delay: Duration,
    next_poll_id: usize,
}

impl UpdatesStream {
    ///  create a new `UpdatesStream` instance.
    pub fn new(api: &Api) -> Self {
        UpdatesStream {
            api: api.clone(),
            last_update: 0,
            buffer: VecDeque::new(),
            timeout: Duration::from_secs(TELEGRAM_LONG_POLL_TIMEOUT_SECONDS),
            allowed_updates: Vec::new(),
            limit: TELEGRAM_LONG_POLL_LIMIT_MESSAGES,
            error_delay: Duration::from_millis(TELEGRAM_LONG_POLL_ERROR_DELAY_MILLISECONDS),
            next_poll_id: 0,
        }
    }

    /// Set timeout for long polling requests, this corresponds with `timeout` field
    /// in [getUpdates](https://core.telegram.org/bots/api#getupdates) method,
    /// also this stream sets an additional request timeout for `timeout + 1 second`
    /// in case of invalid Telegram API server behaviour.
    ///
    /// Default timeout is 5 seconds.
    pub fn timeout(&mut self, timeout: Duration) -> &mut Self {
        self.timeout = timeout;
        self
    }

    /// Set allowed updates to receive, this corresponds with `allowed_updates` field
    /// in [getUpdates](https://core.telegram.org/bots/api#getupdates) method.
    /// List the types of updates you want your bot to receive. For example,
    /// specify [“message”, “edited_channel_post”, “callback_query”] to only receive updates of these types.
    /// See Update for a complete list of available update types. Specify an empty list to receive all
    /// updates regardless of type (default). If not specified, the previous setting will be used.
    ///
    /// Please note that this parameter doesn't affect updates created before the call to the getUpdates,
    /// so unwanted updates may be received for a short period of time.
    pub fn allowed_updates(&mut self, allowed_updates: &[AllowedUpdate]) -> &mut Self {
        self.allowed_updates = allowed_updates.to_vec();
        self
    }

    /// Set limits the number of updates to be retrieved, this corresponds with `limit` field
    /// in [getUpdates](https://core.telegram.org/bots/api#getupdates) method.
    /// Values between 1—100 are accepted.
    ///
    /// Defaults to 100.
    pub fn limit(&mut self, limit: Integer) -> &mut Self {
        self.limit = limit;
        self
    }

    /// Set a delay between erroneous request and next request.
    /// This delay prevents busy looping in some cases.
    ///
    /// Default delay is 500 ms.
    pub fn error_delay(&mut self, delay: Duration) -> &mut Self {
        self.error_delay = delay;
        self
    }

    pub async fn next(&mut self) -> Option<Update> {
        let poll_id = self.next_poll_id;
        self.next_poll_id += 1;
        let span = tracing::trace_span!("stream", poll_id = poll_id);
        let _enter = span.enter();

        tracing::trace!("start stream polling");

        if let Some(value) = self.buffer.pop_front() {
            //println!("STREAM: Found buffered update ({} remaining in buffer)", &self.buffer.len());
            tracing::trace!(update = ?value, "returning buffered update");
            return Some(value);
        }
        tracing::trace!("processing request");

        //println!("STREAM: Sending GetUpdates request");
        let timeout = self.timeout + Duration::from_secs(5);
        let mut get_updates = GetUpdates::new();
        get_updates
            .offset(self.last_update + 1)
            .timeout(self.error_delay.as_secs() as Integer)
            .limit(self.limit)
            .allowed_updates(&self.allowed_updates);
        let request = self.api.send_timeout(get_updates, timeout).await;
        if request.is_err() {
            let err = request.err().unwrap();
            println!("STREAM: Response is error ({})", err);
            return None;
        }
        let request = request.unwrap();
        match request {
            None => {
                //println!("STREAM: No new requests");
                tracing::trace!("there is no current request");
            }
            Some(current_request) => {
                //println!("STREAM: {} new requests", &current_request.len());
                for request in current_request {
                    self.last_update = max(request.id, self.last_update);
                    self.buffer.push_back(request);
                }
            }
        }

        let popped = self.buffer.pop_front();
        if let Some(update) = popped {
            //println!("STREAM: Have update to return");
            Some(update)
        } else {
            //println!("STREAM: No update to return");
            None
        }
    }
}
