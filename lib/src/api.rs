use std::sync::{
    atomic::{AtomicUsize, Ordering},
    Arc, Mutex,
};
use std::{path::Path, time::Duration, fs::read, fmt::Debug};

use futures::{Future, FutureExt};
use tokio::time::timeout;
use tracing_futures::Instrument;

use telegram_bot_raw::{HttpRequest, Request, ResponseType};

use crate::errors::{Error, ErrorKind};
use crate::{connector::hyper::{default_connector, HyperConnector}, stream::UpdatesStream};
use reqwest::{multipart::{Form, Part}, Client};
use serde::de::DeserializeOwned;

/// Main type for sending requests to the Telegram bot API.
#[derive(Clone)]
pub struct Api(Arc<ApiInner>);

struct ApiInner {
    token: String,
    connector: HyperConnector,
    next_request_id: AtomicUsize,
}

impl Api {
    /// Create a new `Api` instance.
    ///
    /// # Example
    ///
    /// Using default connector.
    ///
    /// ```rust
    /// use telegram_bot::Api;
    ///
    /// # fn main() {
    /// # let telegram_token = "token";
    /// let api = Api::new(telegram_token);
    /// # }
    /// ```
    pub fn new<T: AsRef<str>>(token: T) -> Self {
        Self::with_connector(token, default_connector().expect("Unable to spawn connector"))
    }

    /// Create a new `Api` instance wtih custom connector.
    pub fn with_connector<T: AsRef<str>>(token: T, connector: HyperConnector) -> Self {
        Api(Arc::new(ApiInner {
            token: token.as_ref().to_string(),
            connector,
            next_request_id: AtomicUsize::new(0),
        }))
    }

    /// Create a stream which produces updates from the Telegram server.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use telegram_bot::Api;
    /// use futures::StreamExt;
    ///
    /// # #[tokio::main]
    /// # async fn main() {
    /// # let api: Api = Api::new("token");
    ///
    /// let mut stream = api.stream();
    /// let update = stream.next().await;
    ///     println!("{:?}", update);
    /// # }
    /// ```
    pub fn stream(&self) -> UpdatesStream {
        UpdatesStream::new(&self)
    }

    /// Send a request to the Telegram server and wait for a response, timing out after `duration`.
    /// Future will resolve to `None` if timeout fired.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use telegram_bot::{Api, GetMe};
    /// # use std::time::Duration;
    /// #
    /// # #[tokio::main]
    /// # async fn main() {
    /// # let telegram_token = "token";
    /// # let api = Api::new(telegram_token);
    /// # if false {
    /// let result = api.send_timeout(GetMe, Duration::from_secs(2)).await;
    /// println!("{:?}", result);
    /// # }
    /// # }
    /// ```
    pub async fn send_timeout<Req: Request>(
        &self,
        request: Req,
        duration: Duration,
    ) -> Result<Option<<Req::Response as ResponseType>::Type>, Error>
    {
        let api = self.clone();
        let request = request.serialize();
        match timeout(
            duration,
            api.send_http_request::<Req::Response>(request.map_err(ErrorKind::from)?),
        )
        .await
        {
            Err(_) => Ok(None),
            Ok(Ok(result)) => Ok(Some(result)),
            Ok(Err(error)) => Err(error),
        }
    }

    /// Send a request to the Telegram server and wait for a response.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use telegram_bot::{Api, GetMe};
    /// #
    /// # #[tokio::main]
    /// # async fn main() {
    /// # let telegram_token = "token";
    /// # let api = Api::new(telegram_token);
    /// # if false {
    /// let result = api.send(GetMe).await;
    /// println!("{:?}", result);
    /// # }
    /// # }
    /// ```
    pub async fn send<Req: Request>(
        &self,
        request: Req,
    ) -> Result<<Req::Response as ResponseType>::Type, Error> {
        let api = self.clone();
        let request = request.serialize();
        api.send_http_request::<Req::Response>(request.map_err(ErrorKind::from)?).await
    }

    pub async fn dispatch(
        &self,
        request: HttpRequest
    ) {
        let api = self.clone();
        api.shoot_http_request(request).await;
    }

    pub fn send_sync<Req: Request>(
        &self,
        request: Req,
        timeout: Duration
    ) -> Result<<Req::Response as ResponseType>::Type, Error>
    {
        let api = self.clone();
        let request = request.serialize();
        api.send_http_request_sync::<Req::Response>(request.map_err(ErrorKind::from)?, timeout)
    }

    pub fn serialize<Req: Request>(
        &self,
        request: Req
    ) -> Option<HttpRequest> {
        let request = request.serialize();
        if let Ok(request) = request {
            Some(request)
        } else {
            None
        }
    }

    pub async fn send_simple<Req: Request, Resp: DeserializeOwned + Debug>(
        request: Req,
        client: &Client,
        api_token: &String
    ) -> Result<Resp, Error> {
        let request = request.serialize();
        if request.is_err() {
            let error = request.unwrap_err();
            return Err(Error::from(ErrorKind::from(error)));
        }

        let request = request.unwrap();
        tracing::trace!(name = %request.name(), body = %request.body, "sending request");

        let builder = match request.method {
            telegram_bot_raw::Method::Get => client.get(&request.url.url(api_token)),
            telegram_bot_raw::Method::Post => client.post(&request.url.url(api_token))
        };

        let request = match request.body {
            telegram_bot_raw::Body::Empty => { builder },
            telegram_bot_raw::Body::Multipart(parts) => { 
                let mut form = Form::new();
                let mut fields = Vec::new();
                for (key, value) in parts {
                    match value {
                        telegram_bot_raw::MultipartValue::Text(text) => {
                            let text = String::from(text.as_str());
                            let part = Part::text(text);
                            fields.push((key, part));
                        },
                        telegram_bot_raw::MultipartValue::Path { path, file_name } => {
                            let file_name = file_name
                            .or_else(|| {
                                AsRef::<Path>::as_ref(&path)
                                    .file_name()
                                    .and_then(|s| s.to_str())
                                    .map(Into::into)
                            })
                            .ok_or(ErrorKind::InvalidMultipartFilename)?;
                            let file_name = String::from(file_name.as_str());
                            let file_contents = read(&file_name);
                            if file_contents.is_err() {
                                let error = file_contents.unwrap_err();
                                let error = format!("{}", error);
                                return Err(Error::from(ErrorKind::HttpSimple(error)));
                            }
                            let file_contents = file_contents.unwrap();
                            let part = Part
                                ::bytes(file_contents)
                                .file_name(file_name);
                            fields.push((key, part));

                        },
                        telegram_bot_raw::MultipartValue::Data { file_name, data } => {
                            let file_name = String::from(file_name.as_str());
                            let bytes = data.to_vec();
                            let part = Part
                                ::bytes(bytes)
                                .file_name(file_name);
                            fields.push((key, part));
                        }
                    }
                }

                for field in fields {
                    form = form.part(field.0, field.1);
                }

                builder.multipart(form)
            },
            telegram_bot_raw::Body::Json(json_string) => { 
                builder.body(json_string)
            },
            telegram_bot_raw::Body::__Nonexhaustive => { builder }
        };

        //println!("Started sending request");
        let send_response = request.send().await;
        //println!("Done sending request");
        if send_response.is_err() {
            let send_error = send_response.unwrap_err();
            return Err(Error::from(ErrorKind::from(format!("{}", send_error))));
        }
        let send_response = send_response.unwrap();
        let response_body = send_response.bytes().await;
        let body = response_body
            .iter()
            .fold(vec![], |mut acc, chunk| -> Vec<u8> {
                acc.extend_from_slice(&chunk);
                acc
            });

        let resp = serde_json::from_slice(&body);
        if resp.is_err() {
            let resp = resp.unwrap_err();
            let message = format!("{}", resp);
            eprintln!("Deserialization failure: {}", &message);
            return Err(Error::from(ErrorKind::HttpSimple(message)));
        }
        let resp: Resp = resp.unwrap();
        Ok(resp)
    }

    async fn send_http_request<Resp: ResponseType>(
        &self,
        request: HttpRequest,
    ) -> Result<Resp::Type, Error> {
        let request_id = self.0.next_request_id.fetch_add(1, Ordering::Relaxed);
        let span = tracing::trace_span!("send_http_request", request_id = request_id);
        async {
            tracing::trace!(name = %request.name(), body = %request.body, "sending request");
            let http_response = self.0.connector.request(&self.0.token, request).await?;
            tracing::trace!(
                response = %match http_response.body {
                    Some(ref vec) => match std::str::from_utf8(vec) {
                        Ok(str) => str,
                        Err(_) => "<invalid utf-8 string>"
                    },
                    None => "<empty body>",
                }, "response received"
            );

            let response = Resp::deserialize(http_response).map_err(ErrorKind::from)?;
            tracing::trace!("response deserialized");
            Ok(response)
        }
        .map(|result| {
            if let Err(ref error) = result {
                tracing::error!(error = %error);
            }
            result
        })
        .instrument(span)
        .await
    }

    pub async fn shoot_http_request(&self, request: HttpRequest) {
        self.0.connector.request(&self.0.token, request).await.expect("Unable to shoot request");
    }

    pub async fn shoot_http_request_simple<Req: Request>(
        request: Req,
        client: Client,
        api_token: String
    ) {
        let request = request.serialize();
        if request.is_err() {
            return;
        }

        let request = request.unwrap();
        //tracing::trace!(name = %request.name(), body = %request.body, "sending request");

        let builder = match request.method {
            telegram_bot_raw::Method::Get => client.get(&request.url.url(&api_token)),
            telegram_bot_raw::Method::Post => client.post(&request.url.url(&api_token))
        };

        let request = match request.body {
            telegram_bot_raw::Body::Empty => { builder },
            telegram_bot_raw::Body::Multipart(parts) => { 
                let mut form = Form::new();
                let mut fields = Vec::new();
                for (key, value) in parts {
                    match value {
                        telegram_bot_raw::MultipartValue::Text(text) => {
                            let text = String::from(text.as_str());
                            let part = Part::text(text);
                            fields.push((key, part));
                        },
                        telegram_bot_raw::MultipartValue::Path { path, file_name } => {
                            let file_name = file_name
                            .or_else(|| {
                                AsRef::<Path>::as_ref(&path)
                                    .file_name()
                                    .and_then(|s| s.to_str())
                                    .map(Into::into)
                            })
                            .ok_or(ErrorKind::InvalidMultipartFilename).unwrap();
                            let file_name = String::from(file_name.as_str());
                            let file_contents = read(&file_name);
                            if file_contents.is_err() {
                                //let error = file_contents.unwrap_err();
                                //let error = format!("{}", error);
                                return;
                            }
                            let file_contents = file_contents.unwrap();
                            let part = Part
                                ::bytes(file_contents)
                                .file_name(file_name);
                            fields.push((key, part));

                        },
                        telegram_bot_raw::MultipartValue::Data { file_name, data } => {
                            let file_name = String::from(file_name.as_str());
                            let bytes = data.to_vec();
                            let part = Part
                                ::bytes(bytes)
                                .file_name(file_name);
                            fields.push((key, part));
                        }
                    }
                }

                for field in fields {
                    form = form.part(field.0, field.1);
                }

                builder.multipart(form)
            },
            telegram_bot_raw::Body::Json(json_string) => { 
                builder.body(json_string)
            },
            telegram_bot_raw::Body::__Nonexhaustive => { builder }
        };

        request.send().await;
    }

    fn send_http_request_sync<Resp: ResponseType>(
        &self,
        request: HttpRequest,
        timeout: Duration
    ) -> Result<Resp::Type, Error> {
        tracing::trace!(name = %request.name(), body = %request.body, "sending request");

        let client = reqwest::blocking::Client::builder()
            .timeout(timeout)
            .build()
            .expect("Unable to build HTTP client");
        let builder = match request.method {
            telegram_bot_raw::Method::Get => client.get(&request.url.url(&self.0.token)),
            telegram_bot_raw::Method::Post => client.post(&request.url.url(&self.0.token))
        };

        let request = match request.body {
            telegram_bot_raw::Body::Empty => { builder },
            telegram_bot_raw::Body::Multipart(parts) => { 
                let mut form = reqwest::blocking::multipart::Form::new();
                let mut fields = Vec::new();
                for (key, value) in parts {
                    match value {
                        telegram_bot_raw::MultipartValue::Text(text) => {
                            let text = String::from(text.as_str());
                            let part = reqwest::blocking::multipart::Part::text(text);
                            fields.push((key, part));
                        },
                        telegram_bot_raw::MultipartValue::Path { path, file_name } => {
                            let file_name = file_name
                            .or_else(|| {
                                AsRef::<Path>::as_ref(&path)
                                    .file_name()
                                    .and_then(|s| s.to_str())
                                    .map(Into::into)
                            })
                            .ok_or(ErrorKind::InvalidMultipartFilename)?;
                            let file_name = String::from(file_name.as_str());
                            let file_contents = read(&file_name);
                            if file_contents.is_err() {
                                let error = file_contents.unwrap_err();
                                let error = format!("{}", error);
                                return Err(Error::from(ErrorKind::HttpSimple(error)));
                            }
                            let file_contents = file_contents.unwrap();
                            let part = reqwest::blocking::multipart::Part
                                ::bytes(file_contents)
                                .file_name(file_name);
                            fields.push((key, part));

                        },
                        telegram_bot_raw::MultipartValue::Data { file_name, data } => {
                            let file_name = String::from(file_name.as_str());
                            let bytes = data.to_vec();
                            let part = reqwest::blocking::multipart::Part
                                ::bytes(bytes)
                                .file_name(file_name);
                            fields.push((key, part));
                        }
                    }
                }

                for field in fields {
                    form = form.part(field.0, field.1);
                }

                builder.multipart(form)
            },
            telegram_bot_raw::Body::Json(json_string) => { 
                builder.body(json_string)
            },
            telegram_bot_raw::Body::__Nonexhaustive => { builder }
        };

        //println!("Started sending request");
        let send_response = request.send();
        //println!("Done sending request");
        if send_response.is_err() {
            let send_error = send_response.unwrap_err();
            return Err(Error::from(ErrorKind::from(format!("{}", send_error))));
        }
        let send_response = send_response.unwrap();
        let response_body = send_response.bytes();
        let body = response_body
            .iter()
            .fold(vec![], |mut acc, chunk| -> Vec<u8> {
                acc.extend_from_slice(&chunk);
                acc
            });

        let resp = Resp::deserialize_body(&body);
        //let resp = serde_json::from_slice(&body);
        if resp.is_err() {
            let resp = resp.err().unwrap();
            let message = format!("{}", resp);
            eprintln!("Deserialization failure: {}", &message);
            return Err(Error::from(ErrorKind::HttpSimple(message)));
        }
        let resp: Resp::Type = resp.unwrap();
        Ok(resp)
    }
}
